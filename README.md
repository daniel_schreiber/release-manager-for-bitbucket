# README #

This tool collects all issues that were fixed in some/the latest release and assigns them a corresponding milestone. In effect this milestone is basis for your release notes.

# Usage #

Can be run from command line via:
```
#!bash

java -cp bitbucket-release-manager/src/main/java com.release_notes_for_bitbucket.Main bitbucket_user password repository_owner repository_name [--version SOME.VERSION]
```

or as a maven plugin:

```
#!xml

<plugin>
    <groupId>com.release_notes_for_bitbucket</groupId>
    <artifactId>bitbucket-release-manager-maven-plugin</artifactId>
    <version>1.0.3</version>
    <configuration>
        <!-- leaving out password here, use command line argument -Drelease-manager.password=YOUR_PASSWORD -->
        <!-- together with -Drelease-manager.user=YOUR_USER -->
        <user>BITBUCKET-USER</user>
        <repoOwner>REPO-OWNER</repoOwner>
        <repoName>REPO-NAME</repoName>
    </configuration>
    <executions>
        <execution>
            <id>create-release-notes-id</id>
            <goals>
                <goal>manage-issues</goal>
            </goals>
            <inherited>false</inherited>
        </execution>
    </executions>
</plugin>

```

this runs by default in `deploy` phase or via:


```
#!bash

mvn org.bitbucket.daniel_schreiber.release-manager-for-bitbucket:bitbucket-release-manager-maven-plugin:manage-issues -Drelease-manager.password=YOUR_PASSWORD
```

## Usage with maven release plugin ##

The plugin integrates with the maven release plugin - i.e. if the release plugin executes the `deploy` phase (or some
other configured phase the release manager is attached to), then the release manager is executed as expected.

There is only on little quirk, if you pass the password (or other configuration parameters) via command line. As the
maven release plugin does not pass properties correctly to the invocations (see
http://mrhaki.blogspot.de/2008/07/passing-d-arguments-along-to-release.html ) there is some work
around necessary:

```
#!bash

mvn release:perform -Dgoals=deploy -Darguments=-Drelease-manager.password=YOUR_PASSWORD
```

