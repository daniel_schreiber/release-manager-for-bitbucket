package com.release_notes_for_bitbucket.release_helper;

import com.release_notes_for_bitbucket.Main;
import com.release_notes_for_bitbucket.fetcher.ChangesetFetcher;
import com.release_notes_for_bitbucket.fetcher.IssueFetcher;
import com.release_notes_for_bitbucket.model.Changeset;
import com.release_notes_for_bitbucket.version_tree.Commit;
import com.release_notes_for_bitbucket.version_tree.CommitTree;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.*;

/**
 * Created by SCHRED on 31.01.2015.
 */
public class ReleaseHelperTest {

    private ReleaseHelper releaseHelper;
    private IssueFetcher issueFetcher;
    private CommitTree commitTree;
    private ChangesetFetcher changeSetFetcher;

    @Before
    public void setUp() {
        issueFetcher = Mockito.mock(IssueFetcher.class);
        commitTree = Mockito.mock(CommitTree.class);
        changeSetFetcher = Mockito.mock(ChangesetFetcher.class);
        releaseHelper = new ReleaseHelper(commitTree, changeSetFetcher, issueFetcher);
    }

    @Test
    public void getCommitsNotInSomeOtherReleaseTest() {
        Changeset releaseChangeset = new Changeset();
        releaseChangeset.setMessage("release commit");
        releaseChangeset.setNode("release");
        List<String> parents = Arrays.asList("parent1", "parent2");
        releaseChangeset.setParents(parents);

        Set<String> predecessorNodes = new HashSet<>();
        predecessorNodes.addAll(parents);

        Commit releaseCommit = new Commit(predecessorNodes, new Date(), releaseChangeset.getNode(), "1.0.0");
        Mockito.when(commitTree.getForChangeset(Matchers.eq(releaseChangeset))).thenReturn(releaseCommit);
        Mockito.when(commitTree.getPredecessors(Mockito.any(Commit.class))).then(new Answer<Set<Commit>>() {
            @Override
            public Set<Commit> answer(InvocationOnMock invocation) throws Throwable {
                Commit commit = (Commit) invocation.getArguments()[0];
                Set<Commit> result = new HashSet<>();
                for (String predecessor : commit.getPredecessorsNodes()) {
                    result.add(new Commit(new HashSet<String>(), new Date(), commit.getId() + "_pred_" + predecessor, null));
                }
                return result;
            }
        });
        Mockito.when(commitTree.getRecursiveSuccessors(Mockito.any(Commit.class))).then(new Answer<List<Commit>>() {
            @Override
            public List<Commit> answer(InvocationOnMock invocation) throws Throwable {
                return Arrays.asList();
            }
        });

        Set<Commit> result = releaseHelper.getCommitsNotInSomeOtherRelease(releaseChangeset);
        System.out.println("Result: " + result);
        Main.printCommits(result);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 2);

    }

}
