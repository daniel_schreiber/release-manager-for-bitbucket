
package com.release_notes_for_bitbucket.model;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Generated("org.jsonschema2pojo")
public class Changeset {

    private String node;
    private List<File> files = new ArrayList<File>();
    private List<Object> branches = new ArrayList<Object>();
    private String rawAuthor;
    private String utctimestamp;
    private String author;
    private String timestamp;
    private String rawNode;
    private List<String> parents = new ArrayList<String>();
    private String branch;
    private String message;
    private Object revision;
    private Long size;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Changeset) {
            return node.equals(((Changeset) obj).getNode());
        }
        return false;
    }

    /**
     * @return The node
     */
    public String getNode() {
        return node;
    }

    /**
     * @param node The node
     */
    public void setNode(String node) {
        this.node = node;
    }

    /**
     * @return The files
     */
    public List<File> getFiles() {
        return files;
    }

    /**
     * @param files The files
     */
    public void setFiles(List<File> files) {
        this.files = files;
    }

    /**
     * @return The branches
     */
    public List<Object> getBranches() {
        return branches;
    }

    /**
     * @param branches The branches
     */
    public void setBranches(List<Object> branches) {
        this.branches = branches;
    }

    /**
     * @return The rawAuthor
     */
    public String getRawAuthor() {
        return rawAuthor;
    }

    /**
     * @param rawAuthor The raw_author
     */
    public void setRawAuthor(String rawAuthor) {
        this.rawAuthor = rawAuthor;
    }

    /**
     * @return The utctimestamp
     */
    public String getUtctimestamp() {
        return utctimestamp;
    }

    /**
     * @param utctimestamp The utctimestamp
     */
    public void setUtctimestamp(String utctimestamp) {
        this.utctimestamp = utctimestamp;
    }

    /**
     * @return The author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author The author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return The rawNode
     */
    public String getRawNode() {
        return rawNode;
    }

    /**
     * @param rawNode The raw_node
     */
    public void setRawNode(String rawNode) {
        this.rawNode = rawNode;
    }

    /**
     * @return The parents
     */
    public List<String> getParents() {
        return parents;
    }

    /**
     * @param parents The parents
     */
    public void setParents(List<String> parents) {
        this.parents = parents;
    }

    /**
     * @return The branch
     */
    public String getBranch() {
        return branch;
    }

    /**
     * @param branch The branch
     */
    public void setBranch(String branch) {
        this.branch = branch;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The revision
     */
    public Object getRevision() {
        return revision;
    }

    /**
     * @param revision The revision
     */
    public void setRevision(Object revision) {
        this.revision = revision;
    }

    /**
     * @return The size
     */
    public Long getSize() {
        return size;
    }

    /**
     * @param size The size
     */
    public void setSize(Long size) {
        this.size = size;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
