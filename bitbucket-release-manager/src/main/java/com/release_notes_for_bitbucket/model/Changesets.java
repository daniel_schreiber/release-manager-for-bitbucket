
package com.release_notes_for_bitbucket.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Changesets {

    private Long count;
    private Object start;
    private Long limit;
    private List<Changeset> changesets = new ArrayList<Changeset>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The count
     */
    public Long getCount() {
        return count;
    }

    /**
     * 
     * @param count
     *     The count
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * 
     * @return
     *     The start
     */
    public Object getStart() {
        return start;
    }

    /**
     * 
     * @param start
     *     The start
     */
    public void setStart(Object start) {
        this.start = start;
    }

    /**
     * 
     * @return
     *     The limit
     */
    public Long getLimit() {
        return limit;
    }

    /**
     * 
     * @param limit
     *     The limit
     */
    public void setLimit(Long limit) {
        this.limit = limit;
    }

    /**
     * 
     * @return
     *     The changesets
     */
    public List<Changeset> getChangesets() {
        return changesets;
    }

    /**
     * 
     * @param changesets
     *     The changesets
     */
    public void setChangesets(List<Changeset> changesets) {
        this.changesets = changesets;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
