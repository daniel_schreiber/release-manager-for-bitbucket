
package com.release_notes_for_bitbucket.model;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "status",
    "priority",
    "title",
    "reported_by",
    "utc_last_updated",
    "responsible",
    "created_on",
    "metadata",
    "content",
    "comment_count",
    "local_id",
    "follower_count",
    "utc_created_on",
    "resource_uri",
    "is_spam"
})
public class Issue {

    @JsonProperty("status")
    private String status;
    @JsonProperty("priority")
    private String priority;
    @JsonProperty("title")
    private String title;
    @JsonProperty("reported_by")
    private ReportedBy reportedBy;
    @JsonProperty("utc_last_updated")
    private String utcLastUpdated;
    @JsonProperty("responsible")
    private Responsible responsible;
    @JsonProperty("created_on")
    private String createdOn;
    @JsonProperty("metadata")
    private Metadata metadata;
    @JsonProperty("content")
    private String content;
    @JsonProperty("comment_count")
    private Long commentCount;
    @JsonProperty("local_id")
    private Long localId;
    @JsonProperty("follower_count")
    private Long followerCount;
    @JsonProperty("utc_created_on")
    private String utcCreatedOn;
    @JsonProperty("resource_uri")
    private String resourceUri;
    @JsonProperty("is_spam")
    private Boolean isSpam;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The status
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The priority
     */
    @JsonProperty("priority")
    public String getPriority() {
        return priority;
    }

    /**
     * 
     * @param priority
     *     The priority
     */
    @JsonProperty("priority")
    public void setPriority(String priority) {
        this.priority = priority;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The reportedBy
     */
    @JsonProperty("reported_by")
    public ReportedBy getReportedBy() {
        return reportedBy;
    }

    /**
     * 
     * @param reportedBy
     *     The reported_by
     */
    @JsonProperty("reported_by")
    public void setReportedBy(ReportedBy reportedBy) {
        this.reportedBy = reportedBy;
    }

    /**
     * 
     * @return
     *     The utcLastUpdated
     */
    @JsonProperty("utc_last_updated")
    public String getUtcLastUpdated() {
        return utcLastUpdated;
    }

    /**
     * 
     * @param utcLastUpdated
     *     The utc_last_updated
     */
    @JsonProperty("utc_last_updated")
    public void setUtcLastUpdated(String utcLastUpdated) {
        this.utcLastUpdated = utcLastUpdated;
    }

    /**
     * 
     * @return
     *     The responsible
     */
    @JsonProperty("responsible")
    public Responsible getResponsible() {
        return responsible;
    }

    /**
     * 
     * @param responsible
     *     The responsible
     */
    @JsonProperty("responsible")
    public void setResponsible(Responsible responsible) {
        this.responsible = responsible;
    }

    /**
     * 
     * @return
     *     The createdOn
     */
    @JsonProperty("created_on")
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * 
     * @param createdOn
     *     The created_on
     */
    @JsonProperty("created_on")
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * 
     * @return
     *     The metadata
     */
    @JsonProperty("metadata")
    public Metadata getMetadata() {
        return metadata;
    }

    /**
     * 
     * @param metadata
     *     The metadata
     */
    @JsonProperty("metadata")
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    /**
     * 
     * @return
     *     The content
     */
    @JsonProperty("content")
    public String getContent() {
        return content;
    }

    /**
     * 
     * @param content
     *     The content
     */
    @JsonProperty("content")
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 
     * @return
     *     The commentCount
     */
    @JsonProperty("comment_count")
    public Long getCommentCount() {
        return commentCount;
    }

    /**
     * 
     * @param commentCount
     *     The comment_count
     */
    @JsonProperty("comment_count")
    public void setCommentCount(Long commentCount) {
        this.commentCount = commentCount;
    }

    /**
     * 
     * @return
     *     The localId
     */
    @JsonProperty("local_id")
    public Long getLocalId() {
        return localId;
    }

    /**
     * 
     * @param localId
     *     The local_id
     */
    @JsonProperty("local_id")
    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    /**
     * 
     * @return
     *     The followerCount
     */
    @JsonProperty("follower_count")
    public Long getFollowerCount() {
        return followerCount;
    }

    /**
     * 
     * @param followerCount
     *     The follower_count
     */
    @JsonProperty("follower_count")
    public void setFollowerCount(Long followerCount) {
        this.followerCount = followerCount;
    }

    /**
     * 
     * @return
     *     The utcCreatedOn
     */
    @JsonProperty("utc_created_on")
    public String getUtcCreatedOn() {
        return utcCreatedOn;
    }

    /**
     * 
     * @param utcCreatedOn
     *     The utc_created_on
     */
    @JsonProperty("utc_created_on")
    public void setUtcCreatedOn(String utcCreatedOn) {
        this.utcCreatedOn = utcCreatedOn;
    }

    /**
     * 
     * @return
     *     The resourceUri
     */
    @JsonProperty("resource_uri")
    public String getResourceUri() {
        return resourceUri;
    }

    /**
     * 
     * @param resourceUri
     *     The resource_uri
     */
    @JsonProperty("resource_uri")
    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }

    /**
     * 
     * @return
     *     The isSpam
     */
    @JsonProperty("is_spam")
    public Boolean getIsSpam() {
        return isSpam;
    }

    /**
     * 
     * @param isSpam
     *     The is_spam
     */
    @JsonProperty("is_spam")
    public void setIsSpam(Boolean isSpam) {
        this.isSpam = isSpam;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
