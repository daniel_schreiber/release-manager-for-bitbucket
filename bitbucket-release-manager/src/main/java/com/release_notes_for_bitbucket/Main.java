package com.release_notes_for_bitbucket;

import com.release_notes_for_bitbucket.version_tree.Commit;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * TODO
 * - Maven integration
 */
public class Main {

    public static void main(String[] args) throws IOException {

        if (args.length < 4) {
            System.out.println("Usage:");
            System.out.println("\njava " + Main.class.getName() + " bitbucket_user password repository_owner repository_name [--version SOME.VERSION]");
            System.out.println("\n(Creates a milestone for the most current version (or the version given) and sets this milestone for all fixed issues in this version.)");
            return;
        }

        String user = args[0];
        String password = args[1];
        String repoOwner = args[2];
        String repoName = args[3];

        List<String> argsList = Arrays.asList(args);
        String version = null;
        if (argsList.indexOf("--version") > 0) {
            version = argsList.get(argsList.indexOf("--version") + 1);
        }

        new AssignIssuesManager(user, password, repoOwner, repoName, version).invoke();
    }

    public static void printCommits(Set<Commit> newCommitsInRelease) {
        System.out.println("Commits for release:");
        for (Commit commit : newCommitsInRelease) {
            System.out.println("Commit: " + commit.getId());
        }
    }


}