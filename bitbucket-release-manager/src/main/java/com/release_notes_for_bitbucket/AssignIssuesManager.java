package com.release_notes_for_bitbucket;

import com.release_notes_for_bitbucket.fetcher.MilestoneFetcher;
import com.release_notes_for_bitbucket.fetcher.VersionFetcher;
import com.release_notes_for_bitbucket.model.Changeset;
import com.release_notes_for_bitbucket.model.Issue;
import com.release_notes_for_bitbucket.model.Milestone;
import com.release_notes_for_bitbucket.model.Version;
import com.release_notes_for_bitbucket.release_helper.ReleaseHelper;
import com.release_notes_for_bitbucket.version_tree.Commit;

import java.io.IOException;
import java.util.Set;
import java.util.regex.Matcher;

public class AssignIssuesManager {
    private String user;
    private String password;
    private String repoOwner;
    private String repoName;
    private String version;

    public AssignIssuesManager(String user, String password, String repoOwner, String repoName, String version) {
        this.user = user;
        this.password = password;
        this.repoOwner = repoOwner;
        this.repoName = repoName;
        this.version = version;
    }

    public void invoke() throws IOException {
        ReleaseHelper releaseHelper = new ReleaseHelper(user, password, repoOwner, repoName);

        //find commit of latest release:
        Changeset releaseCommit = releaseHelper.getReleaseCommit(version);
        if (releaseCommit == null) {
            System.out.println("ERROR: Could not find release!");
            return;
        }
        System.out.println("Release commit: " + releaseCommit.getMessage());

        //extract release version
        Matcher versionMatcher = Commit.RELEASE_PATTERN.matcher(releaseCommit.getMessage());
        versionMatcher.matches();
        String artefact = versionMatcher.group(1);
        String actualVersion = versionMatcher.group(2);
        System.out.println("Version: " + actualVersion + " (artefact: " + artefact + ")");

        // create Milestone for release
        Milestone milestone = new MilestoneFetcher(user, password, repoOwner, repoName).getOrCreateMilestone(actualVersion);
        System.out.println("Milestone: " + milestone.getName() + ", " + milestone.getId());

        // create Version for release
        Version issueMgmtVersion = new VersionFetcher(user, password, repoOwner, repoName).getOrCreateVersion(actualVersion);
        System.out.println("Version: " + issueMgmtVersion.getName() + ", " + issueMgmtVersion.getId());

        Set<Commit> newCommitsInRelease = releaseHelper.getCommitsNotInSomeOtherRelease(releaseCommit);
        //printCommits(newCommitsInRelease);

        //find all fixed issues for these commits
        Set<Integer> markedAsResolvedIssues = releaseHelper.getResolvedIssues(newCommitsInRelease);
        Set<Issue> fixedIssues = releaseHelper.getFixedIssues(markedAsResolvedIssues);
        //TODO find issues that reference a commit (reference one of the commits in this release and are closed? )
        releaseHelper.setMilestoneForIssues(milestone, fixedIssues);

        System.out.println("View milestone: https://bitbucket.org/" + repoOwner + "/" + repoName + "/issues?milestone=" + milestone.getName());
    }
}
