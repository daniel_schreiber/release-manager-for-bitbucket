package com.release_notes_for_bitbucket.version_tree;

import com.release_notes_for_bitbucket.fetcher.ChangesetFetcher;
import com.release_notes_for_bitbucket.model.Changeset;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Commit {

    public static final Pattern RELEASE_PATTERN = Pattern.compile("\\[maven-release-plugin\\] prepare release ?(.*?)-([0-9]+\\.[0-9]+\\.[0-9]+)\n");
    private final String id;
    /**
     * Commits before this commit (=parents).
     * Is always complete.
     */
    private final Set<Commit> predecessors = new HashSet<>();

    private final Set<String> predecessorsNodes = new HashSet<>();

    private final String release;
    /**
     * Commits after this commit.
     * Might be incomplete.
     */
    private final Set<Commit> successors = new HashSet<>();
    private final Date date;
    private final Changeset changeset;

    public Commit(Changeset changeset) {
        predecessorsNodes.addAll(changeset.getParents());
        this.changeset = changeset;
        try {
            date = ChangesetFetcher.DATE_FORMAT.parse(changeset.getTimestamp());
        } catch (ParseException e) {
            throw new RuntimeException("Could not parse date (" + changeset.getTimestamp() + ")", e);
        }
        id = changeset.getNode();
        Matcher matcher = RELEASE_PATTERN.matcher(changeset.getMessage());
        release = matcher.matches() ? matcher.group(2) : null;
    }

    public Commit(Set<String> predecessorsNodes, Date date, String id, String release) {
        this.predecessorsNodes.addAll(predecessorsNodes);
        this.date = date;
        this.id = id;
        this.release = release;
        this.changeset = null;
    }

    public Set<Commit> getPredecessors() {
        return predecessors;
    }

    public Set<String> getPredecessorsNodes() {
        return predecessorsNodes;
    }

    public Set<Commit> getSuccessors() {
        return successors;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Commit) {
            return this.id.equals(((Commit) obj).id);
        }
        return false;
    }

    public String getRelease() {
        return release;
    }

    public String getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public Changeset getChangeset() {
        return changeset;
    }
}
