package com.release_notes_for_bitbucket.version_tree;

import com.release_notes_for_bitbucket.fetcher.ChangesetFetcher;
import com.release_notes_for_bitbucket.model.Changeset;

import java.io.IOException;
import java.util.*;

/**
 * Created by SCHRED on 17.01.2015.
 */
public class CommitTree {

    private final ChangesetFetcher fetcher;

    /**
     * node -> commit
     */
    private final Map<String, Commit> commits = new HashMap<>();

    public CommitTree(ChangesetFetcher changesetFetcher) {
        this.fetcher = changesetFetcher;

        List<Changeset> changesets = changesetFetcher.getAllFetchedChangesets();
        addToTree(changesets);
    }

    private void addToTree(List<Changeset> changesets) {
        for (Changeset changeset : changesets) {
            if (!commits.containsKey(changeset.getNode())) {
                commits.put(changeset.getNode(), new Commit(changeset));
            }
        }
        for (Commit commit : commits.values()) {
            for (String predecessorNode : commit.getPredecessorsNodes()) {
                if (commits.containsKey(predecessorNode)) {
                    link(commits.get(predecessorNode), commit);
                }
            }
        }
    }

    /**
     * fetches more commits if necessary.
     *
     * @param commit is assumed to be managed by this CommitTree
     * @return
     */
    public Set<Commit> getPredecessors(Commit commit) {
        try {
            int counter = 0;
            while (commit.getPredecessorsNodes().size() != commit.getPredecessors().size()) {

                List<Changeset> newCommits = fetcher.fetchMore(50);
                addToTree(fetcher.getAllFetchedChangesets()); //TODO optimize (this assures we dont miss commits)

                if (counter++ > 10) {
                    throw new RuntimeException("Stopped fetching commits after 10 iterations!");
                }
            }
            return commit.getPredecessors();
        } catch (IOException e) {
            throw new RuntimeException("Could not fetch commit predecessors!", e);
        }
    }

    /**
     * fetches more commits if necessary.
     *
     * @param commit is assumed to be managed by this CommitTree
     * @return
     */
    public Set<Commit> getSuccessors(Commit commit) {
        return commit.getSuccessors(); //TODO check if this is always complete!

    }


    private static void link(Commit predecessor, Commit successor) {
        predecessor.getSuccessors().add(successor);
        successor.getPredecessors().add(predecessor);
    }

    public Commit getForChangeset(Changeset changeset) {
        if (!commits.containsKey(changeset.getNode())) {
            addToTree(Collections.singletonList(changeset));
        }
        return commits.get(changeset.getNode());
    }

    public List<Commit> getRecursiveSuccessors(Commit candidate) {
        List<Commit> result = new ArrayList<>();
        result.addAll(getSuccessors(candidate));
        for (int i = 0; i < result.size(); i++) {
            for (Commit successor : result.get(i).getSuccessors()) {
                if (!result.contains(successor)) {
                    result.add(successor);
                }
            }
        }
        return result;
    }
}
