package com.release_notes_for_bitbucket.fetcher;

import com.fasterxml.jackson.core.type.TypeReference;
import com.release_notes_for_bitbucket.model.Milestone;
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;

import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.List;

/**
 * Created by SCHRED on 18.01.2015.
 */
public class MilestoneFetcher extends Fetcher {
    public MilestoneFetcher(String user, String password, String repoOwner, String repoName) {
        super(user, password, repoOwner, repoName);
    }

    public Milestone createMilestone(String name) throws IOException {
        MultivaluedMap<String, String> params = new MultivaluedStringMap();
        params.add("name", name);
        return invoke("https://bitbucket.org/api/1.0/repositories/{repoOwner}/{repoName}/issues/milestones", "POST", Milestone.class, params, null);
    }

    public Milestone getOrCreateMilestone(String name) throws IOException {
        List<Milestone> existing = invoke("https://bitbucket.org/api/1.0/repositories/{repoOwner}/{repoName}/issues/milestones", "GET", new TypeReference<List<Milestone>>(){});
        for(Milestone m : existing) {
            if(name.equals(m.getName())) {
                return m;
            }
        }
        return createMilestone(name);
    }
}
