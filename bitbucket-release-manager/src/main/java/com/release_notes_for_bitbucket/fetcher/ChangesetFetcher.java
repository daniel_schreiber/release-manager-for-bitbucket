package com.release_notes_for_bitbucket.fetcher;

import com.release_notes_for_bitbucket.model.Changeset;
import com.release_notes_for_bitbucket.model.Changesets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by SCHRED on 17.01.2015.
 */
public class ChangesetFetcher extends Fetcher {

    private static ChangesetFetcher instance;
    private long total = Long.MAX_VALUE;

    /**
     * Contract: always sorted newest/latest to oldest.
     */
    private final List<Changeset> fetchedChangesets = new ArrayList<>();

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("y-M-d HH:mm:ss");

    public static final Comparator<Changeset> NEW_TO_OLD = new Comparator<Changeset>() {
        @Override
        public int compare(Changeset o1, Changeset o2) {
            try {
                return -DATE_FORMAT.parse(o1.getTimestamp()).compareTo(DATE_FORMAT.parse(o2.getTimestamp()));
            } catch (ParseException e) {
                e.printStackTrace();
                throw new RuntimeException("Could not parse commit date!", e);
            }
        }
    };

    public ChangesetFetcher(String user, String password, String repoOwner, String repoName) {
        super(user, password, repoOwner, repoName);
    }

    /**
     * @param limit amount of Changesets to fetch
     * @return newly fetched Changesets
     */
    public List<Changeset> fetchMore(int limit) throws IOException {
        if (total <= fetchedChangesets.size()) {
            return Collections.emptyList();
        }
        Map<String, String> queryParams = new HashMap<>(2);
        Changeset oldestCommit = fetchedChangesets.size() > 0 ? fetchedChangesets.get(fetchedChangesets.size() - 1) : null;
        if (oldestCommit != null) {
            queryParams.put("start", oldestCommit.getNode());
        }
        queryParams.put("limit", String.valueOf(limit));
        Changesets changesets = invoke("https://bitbucket.org/api/1.0/repositories/{repoOwner}/{repoName}/changesets/",
                "GET", Changesets.class, null, queryParams);
        total = changesets.getCount();
        List<Changeset> newChangesets = changesets.getChangesets();
        if (oldestCommit != null && !newChangesets.remove(oldestCommit)) {
            System.err.println("Result did not contain expected commit!");
        }
        fetchedChangesets.addAll(newChangesets);
        Collections.sort(fetchedChangesets, NEW_TO_OLD);
        return newChangesets;
    }

    public List<Changeset> getAllFetchedChangesets() {
        return fetchedChangesets;
    }

    public long getTotal() {
        return total;
    }
}
