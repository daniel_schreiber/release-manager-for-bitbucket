package com.release_notes_for_bitbucket.fetcher;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.glassfish.jersey.client.JerseyWebTarget;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

public abstract class Fetcher {

    private final HttpAuthenticationFeature auth;

    private final String repoOwner;
    private final String repoName;

    public Fetcher(String user, String password, String repoOwner, String repoName) {
        auth = HttpAuthenticationFeature.basic(user, password);
        this.repoName = repoName;
        this.repoOwner = repoOwner;
    }

    protected <T> T invoke(String url, String method, Class<T> returnType) throws IOException {
        return invoke(url, method, returnType, null, null);
    }

    protected <T> T invoke(String url, String method, TypeReference<T> returnType) throws IOException {
        return invoke(url, method, returnType, null, null);
    }

    protected <T> T invoke(String url, String method, final Class<T> returnType, MultivaluedMap<String, String> formParams, Map<String, String> queryParams) throws IOException {
        return invoke(url, method, new TypeReference<T>() {
            @Override
            public Type getType() {
                return returnType;
            }
        }, formParams, queryParams);
    }

    protected <T> T invoke(String url, String method, TypeReference<T> returnType, MultivaluedMap<String, String> formParams, Map<String, String> queryParams) throws IOException {
        url = url.replace("{repoOwner}", repoOwner).replace("{repoName}", repoName);
        ClientConfig configuration = new ClientConfig();
        JerseyClient client = JerseyClientBuilder.createClient(configuration);
        client.register(auth);
        JerseyWebTarget target = client.target(url);
        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                target = target.queryParam(key, queryParams.get(key));
            }
        }
        Response response = target.request("application/json").build(method, formParams != null ? Entity.form(formParams) : null).invoke();
        //System.out.println("Status: " + response.getStatus());
        String result = response.readEntity(String.class);
        //System.out.println("Result (string): " + result);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapper.readValue(result, returnType);
    }
}
