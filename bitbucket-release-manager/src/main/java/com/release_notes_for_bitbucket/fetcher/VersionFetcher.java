package com.release_notes_for_bitbucket.fetcher;

import com.fasterxml.jackson.core.type.TypeReference;
import com.release_notes_for_bitbucket.model.Milestone;
import com.release_notes_for_bitbucket.model.Version;
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;

import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.List;

public class VersionFetcher extends Fetcher {
    public VersionFetcher(String user, String password, String repoOwner, String repoName) {
        super(user, password, repoOwner, repoName);
    }

    public Version createVersion(String name) throws IOException {
        MultivaluedMap<String, String> params = new MultivaluedStringMap();
        params.add("name", name);
        return invoke("https://bitbucket.org/api/1.0/repositories/{repoOwner}/{repoName}/issues/versions", "POST", Version.class, params, null);
    }

    public Version getOrCreateVersion(String name) throws IOException {
        List<Version> existing = invoke("https://bitbucket.org/api/1.0/repositories/{repoOwner}/{repoName}/issues/versions", "GET", new TypeReference<List<Version>>(){});
        for(Version version : existing) {
            if(name.equals(version.getName())) {
                return version;
            }
        }
        return createVersion(name);
    }
}
