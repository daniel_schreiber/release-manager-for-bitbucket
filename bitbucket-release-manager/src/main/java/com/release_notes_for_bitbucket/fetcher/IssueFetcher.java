package com.release_notes_for_bitbucket.fetcher;

import com.release_notes_for_bitbucket.model.Issue;
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;

import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;

/**
 * Created by SCHRED on 18.01.2015.
 */
public class IssueFetcher extends Fetcher {
    public IssueFetcher(String user, String password, String repoOwner, String repoName) {
        super(user, password, repoOwner, repoName);
    }

    public Issue fetchIssue(Integer id) throws IOException {
        return invoke("https://bitbucket.org/api/1.0/repositories/{repoOwner}/{repoName}/issues/" + id, "GET", Issue.class);
    }

    public Issue setMilestone(Long issueId, String milestone) throws IOException {
        MultivaluedMap<String, String> query = new MultivaluedStringMap();
        query.add("milestone", milestone);
        return invoke("https://bitbucket.org/api/1.0/repositories/{repoOwner}/{repoName}/issues/" + issueId, "PUT", Issue.class, query, null);
    }
}
