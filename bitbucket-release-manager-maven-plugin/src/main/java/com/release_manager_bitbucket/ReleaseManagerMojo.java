package com.release_manager_bitbucket;

import com.release_notes_for_bitbucket.AssignIssuesManager;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;


@Mojo(name = "manage-issues", requiresOnline = true, defaultPhase = LifecyclePhase.DEPLOY, aggregator = true)
public class ReleaseManagerMojo extends AbstractMojo {

    /**
     * bitbucket user
     */
    @Parameter(property = "release-manager.user", required = true)
    private String user;
    /**
     * bitbucket user password
     */
    @Parameter(property = "release-manager.password", required = true)
    private String password;

    /**
     * bitbucket repository owner (defaults to 'bitbucket user')
     */
    @Parameter(property = "release-manager.repo.owner", required = false)
    private String repoOwner;

    /**
     * bitbucket repository name
     */
    @Parameter(property = "release-manager.repo.name", required = true)
    private String repoName;


    /**
     * version to assign issues for (leave empty for latest)
     */
    @Parameter(property = "release-manager.version")
    private String version;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        getLog().info("Managing issues for bitbucket..");

        if (repoOwner == null) {
            repoOwner = user;
        }

        AssignIssuesManager manager = new AssignIssuesManager(user, password, repoOwner, repoName, "".equals(version) ? null : version);

        try {
            manager.invoke();
        } catch (IOException e) {
            throw new MojoExecutionException("Could not manage issues: " + e.getLocalizedMessage(), e);
        }

        getLog().info("Done managing issues for bitbucket.");
    }
}
